package com.rookie.springcloud.feignconsumerfirst.controller;

import com.rookie.springcloud.miscroserviceapi.entities.Dept;
import com.rookie.springcloud.miscroserviceapi.service.DeptClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author linhaiping
 * date 2020-02-29
 */
@RestController
public class LoadBalanceTest {

    @Autowired
    private DeptClientService deptClientService;

    @RequestMapping(value = "/consumer/dept/get/{id}")
    public Dept get(@PathVariable("id") Long id) {
        return deptClientService.get(id);
    }

    @RequestMapping(value = "/consumer/dept/list")
    public List<Dept> list() {

        List<Dept> list = deptClientService.list();
        return list;
    }

    @PostMapping(value = "/consumer/dept/add")
    public Object add(Dept dept) {
        return deptClientService.add(dept);
    }
}
