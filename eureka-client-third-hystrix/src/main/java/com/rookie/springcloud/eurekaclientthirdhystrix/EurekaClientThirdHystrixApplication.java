package com.rookie.springcloud.eurekaclientthirdhystrix;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
//开启熔断机制的支持
@EnableCircuitBreaker
@MapperScan(basePackages = {"com.rookie.springcloud.eurekaclientthirdhystrix.dao"})
public class EurekaClientThirdHystrixApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurekaClientThirdHystrixApplication.class, args);
    }

}
