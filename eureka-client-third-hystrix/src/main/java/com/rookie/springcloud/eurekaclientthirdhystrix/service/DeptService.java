package com.rookie.springcloud.eurekaclientthirdhystrix.service;

import com.rookie.springcloud.miscroserviceapi.entities.Dept;

import java.util.List;


public interface DeptService {

    public boolean add(Dept dept);

    public Dept get(Long id);

    public List<Dept> list();
}
