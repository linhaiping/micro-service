package com.rookie.springcloud.microservicesconfigclient.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author linhaiping
 * <p>
 * RefreshScope 该注解会同步更新git文件的配置信息
 */
@RestController
@RefreshScope
public class ClientRestConfig {

    @Value("${spring.application.name}")
    private String applicationName;

    @Value("${server.port}")
    private String port;

    @Value("${eureka.client.serviceUrl.defaultZone}")
    private String eurekaServers;

    @RequestMapping("/config")
    public String getConfig() {

        System.out.println("server port: " + port);
        System.out.println("application name: " + applicationName);
        System.out.println("eureka uri: " + eurekaServers);
        String string = "applicationName: " + applicationName + "\t eurekaServers:" + eurekaServers + "\t port: " + port;
        return string;
    }
}
