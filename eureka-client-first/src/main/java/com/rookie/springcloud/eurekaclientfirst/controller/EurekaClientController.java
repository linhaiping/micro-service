package com.rookie.springcloud.eurekaclientfirst.controller;

import com.rookie.springcloud.eurekaclientfirst.service.DeptService;
import com.rookie.springcloud.miscroserviceapi.entities.Dept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author linhaiping
 * date 2020-02-29
 */
@RestController
public class EurekaClientController {

    @Autowired
    private DeptService service;
    @Autowired
    private DiscoveryClient client;

    @PostMapping(value = "/dept/add")
    public boolean add(@RequestBody Dept dept) {
        return service.add(dept);
    }

    @GetMapping(value = "/dept/get/{id}")
    public Dept get(@PathVariable("id") Long id) {
        return service.get(id);
    }

    @GetMapping(value = "/dept/list")
    public List<Dept> list() {
        return service.list();
    }

}
