/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50721
 Source Host           : localhost
 Source Database       : cloudDB01

 Target Server Type    : MySQL
 Target Server Version : 50721
 File Encoding         : utf-8

 Date: 03/01/2020 02:29:35 AM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `dept`
-- ----------------------------
DROP TABLE IF EXISTS `dept`;
CREATE TABLE "dept" (
  "deptno" bigint(20) NOT NULL AUTO_INCREMENT,
  "dname" varchar(60) DEFAULT NULL,
  "db_source" varchar(60) DEFAULT NULL,
  PRIMARY KEY ("deptno")
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `dept`
-- ----------------------------
BEGIN;
INSERT INTO `dept` VALUES ('1', '开发部', 'clouddb01'), ('2', '人事部', 'clouddb01'), ('3', '财务部', 'clouddb01'), ('4', '市场部', 'clouddb01'), ('5', '运维部', 'clouddb01');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
