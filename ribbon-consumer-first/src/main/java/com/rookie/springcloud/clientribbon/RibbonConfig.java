package com.rookie.springcloud.clientribbon;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author linhaiping
 * date 2020-03-01
 * <p></p>
 * @Description 当前配置类不能放在@ComponentScan所扫描的当前包和子包下
 * 在spring boot 的项目中，相当于不能与启动类同包或其所在包的自包
 */

@Configuration
public class RibbonConfig {

    /**
     * 定制负载均衡规则
     * 默认为:轮询
     * <p>
     * RoundRobinRule 轮询
     * RandomRule     随机
     * RetryRule
     * 先按照轮询寻找，某个服务down之后，还是是会在指定时间内继续尝试继续获取,但是最后会只在可用服务之间调用
     * <p>
     * <p>
     * 可实现IRule接口自定义规则
     */
    @Bean
    public IRule getRule() {
        return new RandomRule();
    }
}
