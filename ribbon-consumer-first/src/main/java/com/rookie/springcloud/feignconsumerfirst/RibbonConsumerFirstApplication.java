package com.rookie.springcloud.feignconsumerfirst;

import com.rookie.springcloud.clientribbon.RibbonConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;

/**
 * @author linhaiping
 */
@SpringBootApplication
@EnableEurekaClient
@RibbonClient(value = "EUREKA-CLIENT", configuration = RibbonConfig.class)
public class RibbonConsumerFirstApplication {

    public static void main(String[] args) {
        SpringApplication.run(RibbonConsumerFirstApplication.class, args);
    }

}
