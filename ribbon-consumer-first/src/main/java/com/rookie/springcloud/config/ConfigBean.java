package com.rookie.springcloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author linhaiping
 * date 2020-02-29
 */
@Configuration
public class ConfigBean {


    /**
     * 只需在bean上添加@LoadBalanced注解
     */
    @Bean
    @LoadBalanced
    public RestTemplate getTemplate() {
        return new RestTemplate();
    }

}
