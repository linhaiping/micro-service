package com.rookie.springcloud.hystrixdashboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

/**
 * @author linhaiping
 */
@SpringBootApplication
@EnableHystrixDashboard
public class HystrixDashBoardApplication {


    /**
     * 注意事项：
     * dashboard 用来监控开启了hystrix熔断机制的服务
     * boot2.X版本的注意点
     * 需要在被监控的服务中添加ServletRegistrationBean 的配置
     * 并在配置文件中暴露hystrix.stream endpoint
     * 详见 eureka-client-third-hystrix服务的配置
     * <p>
     * 我的这个项目中监控的是eureka-client-third-hystrix这个服务
     */
    public static void main(String[] args) {
        SpringApplication.run(HystrixDashBoardApplication.class, args);
    }

}
