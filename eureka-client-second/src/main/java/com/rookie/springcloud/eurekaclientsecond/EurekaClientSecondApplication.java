package com.rookie.springcloud.eurekaclientsecond;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan(basePackages = "com.rookie.springcloud.eurekaclientsecond.dao")
public class EurekaClientSecondApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurekaClientSecondApplication.class, args);
    }

}
