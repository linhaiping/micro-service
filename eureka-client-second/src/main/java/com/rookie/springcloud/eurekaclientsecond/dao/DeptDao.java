package com.rookie.springcloud.eurekaclientsecond.dao;

import com.rookie.springcloud.miscroserviceapi.entities.Dept;

import java.util.List;

/**
 * @author linhaiping
 */
public interface DeptDao {

    public boolean addDept(Dept dept);

    public Dept findById(Long id);

    public List<Dept> findAll();
}
