package com.rookie.springcloud.eurekaclientsecond.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author linhaiping
 * date 2020-02-29
 */
@RestController
public class DiscoveryClientController {

    @Autowired
    DiscoveryClient discoveryClient;

    @Autowired
    @GetMapping(value = "/client/discovery")
    public Object discovery() {
        List<String> list = discoveryClient.getServices();
        System.out.println("***** All Client *****");
        System.out.println(list);

        List<ServiceInstance> srvList = discoveryClient.getInstances("EUREKA-CONTROLLER-FIRST");
        for (ServiceInstance element : srvList) {
            System.out.println(element.getServiceId() + "\t" + element.getHost() + "\t" + element.getPort() + "\t"
                    + element.getUri());
        }

        return this.discoveryClient;
    }

}
